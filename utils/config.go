package utils

import "github.com/spf13/viper"

//CONFIG

var CONFIG *Configuration // 全局

type Configuration struct {
	Service   ServiceConfiguration
	DB        DBConfiguration
	DeBugMode bool
}

type ServiceConfiguration struct {
	Port string
}

type DBConfiguration struct {
	Host     string
	DBType   string
	DBName   string
	User     string
	Password string
	Port     string
	LogMode  string
}

// NewConfiguration ...
func NewConfiguration(configName string, configPaths []string) error {
	v := viper.New()
	v.SetConfigName(configName)
	v.AutomaticEnv()
	for _, configPath := range configPaths {
		v.AddConfigPath(configPath)
	}

	if err := v.ReadInConfig(); err != nil {
		return err
	}

	err := v.Unmarshal(&CONFIG)
	if err != nil {
		return err
	}

	return nil
}
