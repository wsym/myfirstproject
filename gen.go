package main

//go:generate rm -f ./rpc/blog/service.twirp.go
//go:generate rm -f ./rpc/blog/service.pb.go
//go:generate protoc --proto_path=./vendor/:. --twirp_out=. --go_out=. ./rpc/blog/service.proto
