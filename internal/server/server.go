package server

import (
	"github.com/jinzhu/gorm"
	pb "gitlab.com/mvalley/blog/rpc/blog"
)

type Server struct {
	UserMap    map[string]*pb.User    //key->user_id
	BlogMap    map[string]*pb.Blog    //key->blog_id
	CommentMap map[string]*pb.Comment //key->comment_id
	MySQL      *gorm.DB
}
