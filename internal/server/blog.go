package server

import (
	"context"
	"encoding/base64"
	"errors"
	"sort"
	"strconv"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/golang/protobuf/ptypes/wrappers"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/mvalley/blog/rpc/blog"
	pb "gitlab.com/mvalley/blog/rpc/blog"
)

func (s *Server) isLegal(userId string) error {
	if s.UserMap[userId] == nil {
		return errors.New("用户不存在！")
	}
	if s.UserMap[userId].Role != blog.BlogUserRole_AUTHOR {
		return errors.New("没有操作权限！")
	}
	return nil
}
func (s *Server) isExist(blogId string) (*pb.Blog, error) {
	b := s.BlogMap[blogId]
	if b == nil {
		return nil, errors.New("博客不存在！")
	}
	return b, nil
}

type Direction int32

const (
	Direction_FORWORD  Direction = iota //向前
	Direction_BACKWORD                  //向后
)

func (s *Server) pagingIsLegal(first, last *wrappers.Int32Value, after, before *wrappers.StringValue) (Direction, int, error) {
	if first != nil && first.Value > 0 && after != nil && last == nil && before == nil {
		cursor, _ := base64.StdEncoding.DecodeString(after.Value) //base64解码
		index, _ := strconv.Atoi(string(cursor))
		return Direction_BACKWORD, index, nil
	}

	if last != nil && last.Value > 0 && before != nil && first == nil && after == nil {
		cursor, _ := base64.StdEncoding.DecodeString(before.Value) //base64解码
		index, _ := strconv.Atoi(string(cursor))
		return Direction_FORWORD, index, nil
	}
	return 0, 0, errors.New("分页参数有误！")
}

func (s *Server) Blogs(ctx context.Context, req *pb.BlogsRequest) (*pb.BlogConnection, error) {

	//分页参数是否合法
	//direction 方向；index游标起始位置
	direction, index, ok := s.pagingIsLegal(req.First, req.Last, req.After, req.Before)
	if ok != nil {
		return nil, ok
	}

	if index < 0 {
		return nil, errors.New("游标不存在！")
	}

	blogs := make([]*pb.Blog, 0, len(s.BlogMap))
	for _, v := range s.BlogMap {
		if req.State != v.State {
			continue
		}
		//公开 || 作者本人
		if req.State == blog.BlogState_PUBLISHED || req.UserId == v.Owner.Id {
			blogs = append(blogs, v)
		}
	}
	//按照时间倒序排序
	sort.SliceStable(blogs, func(i, j int) bool {
		t1 := blogs[i].CreatedAt
		t2 := blogs[j].CreatedAt
		if blogs[i].UpdatedAt != nil {
			t1 = blogs[i].UpdatedAt
		}
		if blogs[j].UpdatedAt != nil {
			t2 = blogs[j].UpdatedAt
		}
		return t1.Seconds >= t2.Seconds
	})

	if index > len(blogs)-1 {
		return nil, errors.New("游标不存在！")
	}

	blogEdges := make([]*pb.BlogEdge, 0, len(blogs))
	pageInfo := &pb.PageInfo{
		HasNextPage:     false,
		HasPreviousPage: false,
	}
	blogConnection := &pb.BlogConnection{
		TotalCount: int32(len(blogs)),
		PageInfo:   pageInfo,
	}
	if direction == Direction_BACKWORD {
		pageInfo.StartCursor = req.After //开始游标
		//index < end
		end := index + int(req.First.Value)
		if end < len(blogs) {
			pageInfo.HasNextPage = true //有下一页
		} else {
			end = len(blogs)
		}
		if index > 0 {
			pageInfo.HasPreviousPage = true //有上一页
		}
		blogs = blogs[index:end] //左闭右开
		for _, b := range blogs {
			bedge := &pb.BlogEdge{
				Node:   b,
				Cursor: base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(index))), //base64编码
			}
			blogEdges = append(blogEdges, bedge)
			index++
		}
		pageInfo.EndCursor = &wrappers.StringValue{Value: base64.StdEncoding.EncodeToString([]byte(string(end)))} //结束游标
	}

	if direction == Direction_FORWORD {
		pageInfo.StartCursor = req.Before //开始游标
		//start < index
		start := index - int(req.Last.Value) + 1
		if start < 0 {
			start = 0
		}
		if start > 0 {
			pageInfo.HasNextPage = true //有下一页
		}
		if index < len(blogs) {
			pageInfo.HasPreviousPage = true //有上一页
		}
		blogs = blogs[start : index+1] //左闭右开
		for _, b := range blogs {
			bedge := &pb.BlogEdge{
				Node:   b,
				Cursor: base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(index))), //base64编码
			}
			blogEdges = append(blogEdges, bedge)
			index--
		}
		pageInfo.EndCursor = &wrappers.StringValue{Value: base64.StdEncoding.EncodeToString([]byte(string(start)))} //结束游标
	}
	blogConnection.Nodes = blogs
	blogConnection.Edges = blogEdges

	return blogConnection, nil
}

func (s *Server) GetBlog(ctx context.Context, req *pb.BlogRequest) (*pb.Blog, error) {
	//博客是否存在
	b, ok := s.isExist(req.Id)
	if ok != nil {
		return nil, ok
	}
	//游客和作者都可以看
	if b.State == blog.BlogState_PUBLISHED {
		return b, nil
	}
	//其它状态的文章只有作者本人才可以看
	if b.Owner.Id == req.UserId {
		return b, nil
	}
	return nil, errors.New("没有操作权限！")
}

func (s *Server) Comments(ctx context.Context, req *pb.CommentsRequest) (*pb.CommentConnection, error) {
	_, ok := s.GetBlog(ctx, &pb.BlogRequest{Id: req.BlogId})
	if ok != nil {
		return nil, ok
	}

	//分页参数是否合法
	//direction 方向；index游标起始位置
	direction, index, ok := s.pagingIsLegal(req.First, req.Last, req.After, req.Before)
	if ok != nil {
		return nil, ok
	}
	if index < 0 {
		return nil, errors.New("游标不存在！")
	}

	comments := make([]*pb.Comment, 0, len(s.CommentMap))
	for _, v := range s.CommentMap {
		comments = append(comments, v)
	}

	//按照时间倒序排序
	sort.SliceStable(comments, func(i, j int) bool {
		t1 := comments[i].CreatedAt
		t2 := comments[j].CreatedAt
		if comments[i].UpdatedAt != nil {
			t1 = comments[i].UpdatedAt
		}
		if comments[j].UpdatedAt != nil {
			t2 = comments[j].UpdatedAt
		}
		return t1.Seconds >= t2.Seconds
	})

	if index > len(comments)-1 {
		return nil, errors.New("游标不存在！")
	}

	commentEdges := make([]*pb.CommentEdge, 0, len(comments))
	pageInfo := &pb.PageInfo{
		HasNextPage:     false,
		HasPreviousPage: false,
	}
	commentConnection := &pb.CommentConnection{
		TotalCount: int32(len(comments)),
		PageInfo:   pageInfo,
	}

	if direction == Direction_BACKWORD {
		pageInfo.StartCursor = req.After //开始游标
		//index < end
		end := index + int(req.First.Value)
		if end < len(comments) {
			pageInfo.HasNextPage = true //有下一页
		} else {
			end = len(comments)
		}
		if index > 0 {
			pageInfo.HasPreviousPage = true //有上一页
		}
		comments = comments[index:end] //左闭右开
		for _, c := range comments {
			cedge := &pb.CommentEdge{
				Node:   c,
				Cursor: base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(index))), //base64编码
			}
			commentEdges = append(commentEdges, cedge)
			index++
		}
		pageInfo.EndCursor = &wrappers.StringValue{Value: base64.StdEncoding.EncodeToString([]byte(string(end)))} //结束游标
	}

	if direction == Direction_FORWORD {
		pageInfo.StartCursor = req.Before //开始游标
		//start < index
		start := index - int(req.Last.Value) + 1
		if start < 0 {
			start = 0
		}
		if start > 0 {
			pageInfo.HasNextPage = true //有下一页
		}
		if index < len(comments) {
			pageInfo.HasPreviousPage = true //有上一页
		}
		comments = comments[start : index+1] //左闭右开
		for _, c := range comments {
			cedge := &pb.CommentEdge{
				Node:   c,
				Cursor: base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(index))), //base64编码
			}
			commentEdges = append(commentEdges, cedge)
			index--
		}
		pageInfo.EndCursor = &wrappers.StringValue{Value: base64.StdEncoding.EncodeToString([]byte(string(start)))} //结束游标
	}
	commentConnection.Nodes = comments
	commentConnection.Edges = commentEdges
	return commentConnection, nil
}

func (s *Server) CreateBlog(ctx context.Context, req *pb.CreateBlogInput) (*pb.Blog, error) {
	if ok := s.isLegal(req.UserId); ok != nil {
		return nil, ok
	}
	u := s.UserMap[req.UserId]
	id := uuid.NewV4().String()
	b := &pb.Blog{
		Id:        id,
		Title:     req.Title,
		Content:   req.Content,
		Tag:       req.Tag,
		State:     req.State,
		CreatedAt: &timestamp.Timestamp{Seconds: time.Now().Unix()},
		Owner:     u,
	}
	s.BlogMap[id] = b
	return b, nil
}

func (s *Server) UpdateBlog(ctx context.Context, req *pb.UpdateBlogInput) (*pb.Blog, error) {
	//博客是否存在
	b, ok := s.isExist(req.Id)
	if ok != nil {
		return nil, ok
	}
	if b.Owner.Id == req.UserId {
		b.Title = req.Title
		b.Tag = req.Tag
		b.Content = req.Content
		b.State = req.State
		b.UpdatedAt = &timestamp.Timestamp{Seconds: time.Now().Unix()}
		return b, nil
	}
	return nil, errors.New("没有操作权限！")
}

func (s *Server) DeleteBlog(ctx context.Context, req *pb.DeleteBlogRequest) (*wrappers.BoolValue, error) {
	b := s.BlogMap[req.Id]
	if b == nil {
		return &wrappers.BoolValue{Value: false}, errors.New("博客不存在！")
	}
	if b.Owner.Id == req.UserId {
		delete(s.BlogMap, req.Id)
		return &wrappers.BoolValue{Value: true}, nil
	}
	return &wrappers.BoolValue{Value: false}, errors.New("没有操作权限！")
}

func (s *Server) CreateUser(ctx context.Context, req *pb.CreateUserInput) (*pb.User, error) {
	id := uuid.NewV4().String()
	u := &pb.User{
		Id:    id,
		Name:  req.Name,
		Email: req.Email,
		Phone: req.Phone,
		Role:  req.Role,
	}
	s.UserMap[id] = u
	return u, nil
}

func (s *Server) AddComment(ctx context.Context, req *pb.AddCommentRequest) (*pb.Comment, error) {
	u := s.UserMap[req.UserId]
	if u == nil {
		return nil, errors.New("用户不存在！")
	}
	//博客是否存在
	_, ok := s.isExist(req.BlogId)
	if ok != nil {
		return nil, ok
	}
	id := uuid.NewV4().String()
	c := &blog.Comment{
		Id:        id,
		BlogId:    req.BlogId,
		UserName:  u.Name,
		Comment:   req.Comment,
		CreatedAt: &timestamp.Timestamp{Seconds: time.Now().Unix()},
		UserId:    req.UserId,
	}
	s.CommentMap[id] = c
	return c, nil
}

func (s *Server) UpdateComment(ctx context.Context, req *pb.UpdateCommentRequest) (*pb.Comment, error) {
	c := s.CommentMap[req.Id]
	if c == nil {
		return nil, errors.New("评论不存在！")
	}
	if c.UserId != req.UserId {
		return nil, errors.New("没有操作权限！")
	}
	c.Comment = req.Comment
	c.UpdatedAt = &timestamp.Timestamp{Seconds: time.Now().Unix()}
	return c, nil
}

func (s *Server) DeleteComment(ctx context.Context, req *pb.DeleteCommentRequest) (*wrappers.BoolValue, error) {
	c := s.CommentMap[req.Id]
	if c == nil {
		return nil, errors.New("评论不存在！")
	}
	//评论者 || 文章作者
	if c.UserId == req.UserId || req.UserId == s.BlogMap[req.BlogId].Owner.Id {
		delete(s.CommentMap, req.Id)
		return &wrappers.BoolValue{Value: true}, nil
	}
	return &wrappers.BoolValue{Value: false}, errors.New("没有操作权限！")
}
