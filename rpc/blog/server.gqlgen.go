package blog

import (
	fmt "fmt"
	io "io"
)

func (r BlogUserRole) MarshalGQL(w io.Writer) {
	fmt.Fprintf(w, "%q", r.String()) //有双引号
	// w.Write([]byte(BlogUserRole_name[int32(r)]))//没双引号
}

func (r *BlogUserRole) UnmarshalGQL(v interface{}) error {
	if str, ok := v.(string); ok {
		*r = BlogUserRole(BlogUserRole_value[str])
		return nil
	}
	return fmt.Errorf("points must be string")
}

func (s BlogState) MarshalGQL(w io.Writer) {
	fmt.Fprintf(w, "%q", s.String()) //有双引号
}

func (s *BlogState) UnmarshalGQL(v interface{}) error {
	if str, ok := v.(string); ok {
		*s = BlogState(BlogState_value[str])
		return nil
	}
	return fmt.Errorf("points must be string")
}
