package scripts

import (
	"fmt"
	"time"

	"gitlab.com/mvalley/blog/utils"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type User struct {
	UserID   uint   `gorm:"PRIMARY_KEY"`
	UserName string `gorm:"type:VARCHAR(255);not null"`
}

type Todo struct {
	TodoID     uint `gorm:"PRIMARY_KEY"`
	UserID     uint `gorm:"ForeignKey:User"`
	Content    string
	Important  int `grom:"type:INT;not null"`
	CreateTime time.Time
}

func main() {

	path := []string{"gitlab.com/", "mvalley/", "blog/", "configs"}
	utils.NewConfiguration("config", path)
	config := utils.CONFIG

	dbConnStr := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local&multiStatements=True",
		config.DB.User, config.DB.Password, config.DB.Host, config.DB.Port, config.DB.DBName)

	fmt.Println("dbConnStr:", dbConnStr)
	// 格式 `userName:password@(host:port)/dbName?charset=utf8&parseTime=True&loc=Local&multiStatements=True`
	//dbConnStr := `root:pacman@(127.0.0.1:3306)/training?charset=utf8&parseTime=True&loc=Local&multiStatements=True`
	db, _ := gorm.Open("mysql", dbConnStr)

	db.LogMode(true)

	//自动创建表格
	db.AutoMigrate(&User{})

	//添加一条记录
	db.Create(&User{Name: "刘备", Price: 1000, CreateTime: time.Now()})

	//读取一条记录
	db.First(&User{}, "Name=?", "马云")

}
