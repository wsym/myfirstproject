module gitlab.com/mvalley/blog

go 1.13

require (
	github.com/golang/protobuf v1.4.2
	github.com/jinzhu/gorm v1.9.15
	github.com/pkg/errors v0.9.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/viper v1.7.0
	github.com/twitchtv/twirp v5.12.1+incompatible
	google.golang.org/protobuf v1.25.0
)
