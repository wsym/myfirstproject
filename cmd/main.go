package main

import (
	"fmt"
	"net/http"

	"gitlab.com/mvalley/blog/internal/server"
	blog_twirp "gitlab.com/mvalley/blog/rpc/blog"
	pb "gitlab.com/mvalley/blog/rpc/blog"
)

func main() {
	server := &server.Server{
		UserMap:    make(map[string]*pb.User, 0),
		BlogMap:    make(map[string]*pb.Blog, 0),
		CommentMap: make(map[string]*pb.Comment, 0),
	}
	twirpHandler := blog_twirp.NewBlogTwirpServer(server, nil)

	fmt.Println("ready perfectly, port:", 1234)
	http.ListenAndServe(":1234", twirpHandler)
}
